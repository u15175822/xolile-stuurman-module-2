void main() {
  //Declaring and initialising variables
  List award_winning_apps = [
    "FNB-2012",
    "SnapScan-2013",
    "LIVE Inspect-2014",
    "WumDrop-2015",
    "Domestly-2016",
    "Shyft-2017",
    "Khula Ecosystem-2018",
    "Naked Insurance-2019",
    "EasyEquities-2020",
    "Ambani Africa-2021"
  ];

  //Sort the award winning apps in alphabetical order
  award_winning_apps.sort();

  //Print the names of the apps in alphabetic order
  print("Award winning apps(in alphabetical order) are $award_winning_apps");

  //Print the winning app of 2017 and the winning app of 2018
  print("The winning app of 2017 is :");
  print(award_winning_apps[7]);
  print("The winning app of 2018 is :");
  print(award_winning_apps[4]);

  //Print total number of apps
  print("The total number of winning apps is :");
  print(award_winning_apps.length);
}
