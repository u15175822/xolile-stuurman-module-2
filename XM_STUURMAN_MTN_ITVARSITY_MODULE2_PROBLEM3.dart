class AwardWinningApps {
  String? name = "Ambani Africa";
  String? category = '''
Best Gaming Solution,Best Educational Solution and Best South Africa Solution''';
  String? developer = "Mukundi Lambani";
  int? year = 2021;

  String? upperCase(String? name) {
    return name?.toUpperCase();
  }
}

void main() {
  var awardWinningApp = AwardWinningApps();
  String? name = awardWinningApp.name;
  print(awardWinningApp.upperCase(name));
  print(awardWinningApp.category);
  print(awardWinningApp.developer);
  print(awardWinningApp.year);
}
